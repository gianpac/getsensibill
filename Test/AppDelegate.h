//
//  AppDelegate.h
//  Test
//
//  Created by Giancarlo Pacheco on 2016-02-11.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

