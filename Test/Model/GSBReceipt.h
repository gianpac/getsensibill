//
//  GSBReceipt.h
//  Test
//
//  Created by Giancarlo Pacheco on 2016-02-11.
//
//

#import <Foundation/Foundation.h>

extern NSString *const  GSBReceiptID;
extern NSString *const  GSBReceiptAmount;
extern NSString *const  GSBReceiptDate;
extern NSString *const  GSBReceiptStoreName;

@interface GSBReceipt : NSObject

/**
 *  Identifier for the receipt.
 */
@property (nonatomic, readonly) NSNumber        *receiptID;

/**
 *  Value for the receipt.
 */
@property (nonatomic, readonly) NSNumber        *receiptAmount;

/**
 *  Date in which the receipt was issued.
 */
@property (nonatomic, readonly) NSTimeInterval  receiptDate;

/**
 *  Name of the store where the receipt was issued.
 */
@property (nonatomic, readonly) NSString        *receiptName;


/**
 *  Initializes the Receipt from a Json response.
 *
 *  @param dictionary The Key-Value pairs that contain info about the receipts.
 */
- (void)updateWithDictionary:(NSDictionary *)dictionary;

/**
 *  @return The string representation of the receiptAmount in the format '$XXX.xx'
 */
- (NSString *)receiptAmountDescription;

/**
 *  @return The String representation of the receiptDate interval in the format 'January 20th, 2016'
 */
- (NSString *)receiptDateDescription;

@end
