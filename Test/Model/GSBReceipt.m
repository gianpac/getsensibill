//
//  GSBReceipt.m
//  Test
//
//  Created by Giancarlo Pacheco on 2016-02-11.
//
//

#import "GSBReceipt.h"

/**
 *  Sample Response
 *
 *  display = {
 *      amount = "$12.57";
 *      date = "January 30th, 2016";
 *      name = "Tim Hortons";
 *  };
 *  id = 1;
 *  receiptAmount = "12.57";
 *  receiptDate = "2016-01-30T18:27:00.000Z";
 *  receiptName = "Tim Hortons";
 */

NSString *const  GSBReceiptID           = @"id";
NSString *const  GSBReceiptAmount       = @"receiptAmount";
NSString *const  GSBReceiptDate         = @"receiptDate";
NSString *const  GSBReceiptStoreName    = @"receiptName";

static  NSDateFormatter *_parseDateFormatter    = nil;
static  NSDateFormatter *_displayDateFormatter  = nil;

@interface GSBReceipt () {
    NSNumber        *_receiptID;
    NSNumber        *_receiptAmount;
    NSTimeInterval  _receiptDate;
    NSString        *_receiptName;
}

@end

@implementation GSBReceipt

+ (void)initialize {
    _parseDateFormatter = [[NSDateFormatter alloc] init];
    [_parseDateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    _displayDateFormatter = [[NSDateFormatter alloc] init];
    [_displayDateFormatter setDateFormat:@"MMMM dd'th', yyyy"];
}

- (void)updateWithDictionary:(NSDictionary *)dictionary {
    id value = dictionary[GSBReceiptID];
    if (value) {
        _receiptID = (NSNumber *)value;
    }
    
    value = dictionary[GSBReceiptAmount];
    if (value) {
        _receiptAmount = (NSNumber *)value;
    }
    
    value = dictionary[GSBReceiptDate];
    if (value) {
        NSDate *date = [_parseDateFormatter dateFromString:(NSString *)value];
        _receiptDate = [date timeIntervalSince1970];
    }
    
    value = dictionary[GSBReceiptStoreName];
    if (value) {
        _receiptName = (NSString *)value;
    }
}

#pragma mark - Display

- (NSString *)receiptAmountDescription {
    return [NSString stringWithFormat:@"$%0.2f", [_receiptAmount floatValue]];
}

- (NSString *)receiptDateDescription {
    return [NSString stringWithFormat:@"%@", [_displayDateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:_receiptDate]]];
}

#pragma mark - 

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@: %p>\rid: %ld\rreceiptAmount: %@\rreceiptDate: %@\rreceiptName: %@", [self class], self, [_receiptID integerValue], [self receiptAmountDescription], [self receiptDateDescription], _receiptName];
}

@end
