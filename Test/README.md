#Overview
This project builds a simple application that pulls a JSON response of Receipt Objects from a provided endpoint. Then it populates a table view based on a design.

#Requirements
- Hit the following [endpoint](https://getsensibill.com/api/tests/receipts)
- Display results in a table sorted alphabetically matching the design.
- Once completed please provide a link to your repo so we can review
your code.

#Target
- Your understanding of Auto Layout and attention to design details.
- Ability to write clean, lean, organized and commented code following
the MVC pattern.
- Usage of dependency management.

#Conclusions
All of the requirements were successfully met. Some questions still came up, but based on the overview ("build a simple app"), I thought they were irrelevant for a test. For example, if the app is loaded with no network connection, it shows an error, but if later a network is acquired, there is no reload mechanism.

As far as Targets, another concern came up with the "usage of dependency management". Not sure if I had to import a library like AFNetworking and show the use of dependencies. Even though this could have solved some of the questions that came up during development, it was just going to deviate from the goal of the project, which is to "build a simple app". Usage of dependency management is therefore not demonstrated in this app. If needed, I can go back and add it.

Other targets were successfully met. Code is kept to a minimal and comments are mostly there for what is needed. Use of Autolayout and attention to details is demonstrated.

One thing to mention is that the JSON response, from the endpoint given, shows somewhat redundant data. The objects in the response array are a little misleading, IMHO. There is a Display object with data that, I think, should have matched the one shown to the user, but in order to parse the response, I found it was unnecessary to parse all of it. Later, trying to match the end result image to the one shown by the app, I saw that the data parsed was not the same as the one on the Display object. I assumed this is a bug on the response data.