//
//  GSBReceiptTableViewCell.h
//  Test
//
//  Created by Giancarlo Pacheco on 2016-02-11.
//
//

#import <UIKit/UIKit.h>

@interface GSBReceiptTableViewCell : UITableViewCell

/**
 *  Sets the Store Name Label as the main title of the cell.
 *
 *  @param name Store Name
 */
- (void)setStoreName:(NSString *)name;

/**
 *  Sets the Receipt Date Label as the subtitle of the cell.
 *
 *  @param date The String representation of the date
 */
- (void)setReceiptDate:(NSString *)date;

/**
 *  Sets the amount label text
 *
 *  @param amount The String representation of the receipt amount
 */
- (void)setReceiptAmount:(NSString *)amount;

@end
