//
//  GSBReceiptTableViewCell.m
//  Test
//
//  Created by Giancarlo Pacheco on 2016-02-11.
//
//

#import "GSBReceiptTableViewCell.h"

@interface GSBReceiptTableViewCell ()

@property (nonatomic, weak)     IBOutlet    UILabel     *storeNameLabel;
@property (nonatomic, weak)     IBOutlet    UILabel     *receiptDateLabel;
@property (nonatomic, weak)     IBOutlet    UILabel     *receiptValueLabel;

@end

@implementation GSBReceiptTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)prepareForReuse {
    self.storeNameLabel.text = nil;
    self.receiptDateLabel.text = nil;
    self.receiptValueLabel.text = nil;
}

#pragma mark - Public

- (void)setStoreName:(NSString *)name {
    self.storeNameLabel.text = name;
}

- (void)setReceiptAmount:(NSString *)amount {
    self.receiptValueLabel.text = amount;
}

- (void)setReceiptDate:(NSString *)date {
    self.receiptDateLabel.text = date;
}

@end
