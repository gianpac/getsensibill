//
//  ViewController.m
//  Test
//
//  Created by Giancarlo Pacheco on 2016-02-11.
//
//

#import "GSBReceiptsTableViewController.h"
#import "GSBServiceManager.h"
#import "GSBReceiptTableViewCell.h"

static NSString *const GSBReceiptTableViewCellIdentifier    = @"receiptTableViewCellIdentifier";

@interface GSBReceiptsTableViewController () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *_receipts;
}

@property (nonatomic, weak)     IBOutlet    UITableView     *tableView;

@end

@implementation GSBReceiptsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = NSLocalizedString(@"Receipts", @"Receipts");
    [self loadReceipts];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Service Manager

- (void)loadReceipts {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [[GSBServiceManager sharedManager] receiptsWithCompletionHandler:^(NSArray *receipts, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:GSBReceiptStoreName ascending:YES];
        _receipts = [receipts sortedArrayUsingDescriptors:@[sort]];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                                message:error.localizedDescription
                                                               delegate:nil
                                                      cancelButtonTitle:NSLocalizedString(@"OK", @"General")
                                                      otherButtonTitles:nil];
                [alert show];
            }
            else {
            
                [self.tableView reloadData];
            }
        });
    }];
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _receipts.count;
}

- (GSBReceiptTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GSBReceiptTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:GSBReceiptTableViewCellIdentifier forIndexPath:indexPath];
    
    GSBReceipt *receipt = [_receipts objectAtIndex:indexPath.row];
    
    [cell setStoreName:receipt.receiptName];
    [cell setReceiptDate:[receipt receiptDateDescription]];
    [cell setReceiptAmount:[receipt receiptAmountDescription]];
    
    return cell;
}

@end
