//
//  GSBServiceManager.h
//  Test
//
//  Created by Giancarlo Pacheco on 2016-02-11.
//
//

#import <Foundation/Foundation.h>
#import "GSBReceipt.h"


/**
 *  This class manages the API calls to getsensibill servers.
 */
@interface GSBServiceManager : NSObject

/**
 *  @return The singleton that handles the communication with the server.
 */
+ (instancetype)sharedManager;

/**
 *  Calls the API to get the receipts from getSensibill server and parses it into GSBReceipt objects
 *
 *  @param completion The block thrown as a result of the API. It may throw an array of receipt objects
 *                      or an error.
 */
- (void)receiptsWithCompletionHandler:(void(^)(NSArray *receipts, NSError *error))completion;

@end
