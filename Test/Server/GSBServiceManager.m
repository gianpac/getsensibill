//
//  GSBServiceManager.m
//  Test
//
//  Created by Giancarlo Pacheco on 2016-02-11.
//
//

#import "GSBServiceManager.h"

static NSString *const GSBReceiptsKey       = @"receipts";
static NSString *const GSBReceiptsPath      = @"https://getsensibill.com/api/tests/receipts";

@interface GSBServiceManager () <NSURLSessionDelegate> {
    NSURLSession                *_session;
}

@end

@implementation GSBServiceManager

+ (instancetype)sharedManager {
    static GSBServiceManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[[self class] alloc] init];
    });
    return _sharedManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSURLSessionConfiguration *sessionConfiguration = [self sessionConfiguration];
        
        _session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                 delegate:self
                                            delegateQueue:nil];
    }
    return self;
}

- (NSURLSessionConfiguration *)sessionConfiguration {
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    [sessionConfiguration setHTTPAdditionalHeaders:@{@"Accept": @"application/json"}];
    sessionConfiguration.timeoutIntervalForRequest = 30.0;

    return sessionConfiguration;
}

#pragma mark - API Calls

- (void)receiptsWithCompletionHandler:(void(^)(NSArray *receipts, NSError *error))completion {
    NSURLSessionDataTask *receiptsTask = [_session dataTaskWithURL:[NSURL URLWithString:GSBReceiptsPath]
                                                 completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                     NSError *err = nil;
                                                     if (error == nil) {
                                                         NSJSONSerialization *json = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                     options:NSJSONReadingAllowFragments
                                                                                                                       error:&err];
                                                         
                                                         if (err == nil) {
                                                             NSDictionary *receiptsArray = ((NSDictionary *)json)[GSBReceiptsKey];
                                                             NSMutableArray *receipts = [NSMutableArray array];
                                                             for (NSDictionary *receipt in receiptsArray) {
                                                                 GSBReceipt *rec = [[GSBReceipt alloc] init];
                                                                 [rec updateWithDictionary:receipt];
                                                                 [receipts addObject:rec];
                                                             }
                                                             
                                                             completion(receipts, nil);
                                                             return;
                                                         }
                                                     }
                                                     else {
                                                         err = error;
                                                     }
                                                     
                                                     completion(nil, err);
                                                 }];
    
    [receiptsTask resume];
}

@end
